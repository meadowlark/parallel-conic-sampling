#!/bin/bash
#
g++ -c typecount.cpp >& compiler.txt
if [ $? -ne 0 ]; then
  echo "Errors compiling emps.cpp"
  exit
fi
rm compiler.txt
#
g++ -o typecount typecount.o
if [ $? -ne 0 ]; then
  echo "Errors loading typecount.o."
  exit
fi
#
rm typecount.o
