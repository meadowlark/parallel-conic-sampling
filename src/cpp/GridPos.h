/*
 * GridPos.h
 *
 *  Created on: Apr 23, 2014
 *      Author: MeadowlarkBradshaw
 */

#ifndef GRIDPOS_H_
#define GRIDPOS_H_

class GridPos {
public:
	int row;
	int col;
	double val;
	GridPos(int, int, double);
};

#endif /* GRIDPOS_H_ */
