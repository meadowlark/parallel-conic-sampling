#ifndef _SIMPLEX_H
#define _SIMPLEX_H

#include "LPSolver.h"

class Simplex : public LPSolver
{
 public:
  Simplex(char * fname);
  Vector solve();

 protected:
  void clear();

  // matrix of constraints and variadbles
 
  void updateBasisIndices(int r, int c);
  void buildTableau();

  Matrix mat;
 
  // total number of constraints
  int K;
  // total number of constraints
  int N;

  Array<int> basisRows, basisColumns;

  void pivot(int p, int q);

  virtual int pivotColumn(const Vector & edges) const = 0;
};

class SteepestSimplex : public Simplex {
 public:
 SteepestSimplex(char * fname) : Simplex(fname) { }
 protected:
  virtual int pivotColumn(const Vector & edges) const {
    return edges.argmin()[0];
  }
};

class RandomizedSimplex : public Simplex {
 public:
 RandomizedSimplex(char * fname) : Simplex(fname) { }
 protected:
  virtual int pivotColumn(const Vector & edges) const {
    return (edges < 0.0) [ rand() % ( (edges < 0.0).size() ) ];
  }
};

class BlandSimplex : public Simplex {
 public:
 BlandSimplex(char * fname) : Simplex(fname) { }
 protected:
  virtual int pivotColumn(const Vector & edges) const {
    return (edges < 0.0) [0];
  }
};

#endif

