#ifndef _ConicSamplingPolytopeProjection_H
#define _ConicSamplingPolytopeProjection_H

//#include "ConicSampling.h"

#include "LPSolver.h"

class ConicSamplingPolytopeProjection : public LPSolver
{
public:
  ConicSamplingPolytopeProjection();
  ConicSamplingPolytopeProjection(char * fname);
  Vector solve();

  void checkFeasibility() const;

  // overrides
  void load(istream & fin);

  void outputMathematica(char * fname) const;
  void printResult() const;
protected:

  void recalculateF();

  bool descentStep();
  
  void clear();

  // returns false if optimal
  bool randomStep();

  int advance(const Vector & p);
  void addConstraint(int index);
  void addConstraintLinearProjection(int index);
  void addConstraintGramSchmidt(int index);

  //  void normalizeLP();
  //  void addPositivityConstraints();

  Vector getProjection() const;

  Matrix getSpanningVectors() const;
  Matrix getOldSpanning();

  Matrix S;
  Matrix SSTransInverse;

  Vector x0;

  Numerical hitChecker;

  Set currentGliders;

  GramSchmidt gs;
};

#endif

