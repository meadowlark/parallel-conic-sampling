#include <iostream>
#include <fstream>
#include <string>
#include <cstring>
#include <map>
#include <stdlib.h>
#include <vector>
#include "GridPos.h"
#include <sstream>

using namespace std;



/*
void doSlack(Tableau* tableau){
	//store old number of columns, will have to use
	//this number to move the RHS column to the end
	int oldColNum = tableau->width() - 1;

	//add columns equals to the number of rows - 1 
	tableau->addCol(tableau->height() - 1);

	//move the RHS column to the end
	tableau->swapCols(oldColNum, tableau->width() - 1);

	//place in the swaps, starts at row=1, col=oldColNum
	for (int row = 1; row < tableau->height(); row++){
		(*tableau)[row][oldColNum + row - 1] = 1.0;
	}

	//you now have a standardized tableau matrix!
}
*/

int main(int argc, char* argv[]) {


	/*
	cout << "argc = " << argc << endl;
	for(int i = 0; i < argc; i++)
		cout << "argv[" << i << "] = " << argv[i] << endl;
	 */
	if(argc==1){
		cout << "Usage: input_MPS_file output_file_name \n";
	}


	ifstream file;
	file.open(argv[1]);
	//file.open("TESTPROB");
	string s;


	map<int, double> cost;
	map<int, string> names;
	map<string, int> colIndx;
	map<string, int> rowIndx;
	map<int, double> rhs;
	map<int, map<int, double> > A;

	int rowCount=0;
	int colCount=0;
	string costName;

	int numRowsRead = 1; //starts with 1 since row 0 is the objective function
	int numColsRead = 1; //numCols is offset by 1, col 1 means its actually col 0


	map<string, int> colMap; //stores a map from the col name to its col number
	map<string, string> ineqMap;
	//map<string, string> bndMap;
	//map<string, int> offsetMap;
	vector<GridPos> matrixPositions; //a list of positions and values for the tableau
	vector<GridPos> tempPos;

	cout << "checking to see if file is open." << endl;
	int lineNum=0;
	int rowOffset = -1;
	if (file.is_open()) {

		cout << "file is indeed open." << endl;
		getline(file, s);
		while (!file.eof()){
			lineNum++;
			//cout << "file at line="<< lineNum<< " is not EOF." << "It is "<< s.size() <<"!"<<endl;

			/*
			 * In ROWS:
			 *
			 * information = length of rhs vector and constraints of rhs
			 */
			if (s.compare(0,4,"ROWS") == 0){
				cout << "in ROWS" << endl;

				//do stuff for ROW
				getline(file, s);
				int first =0;

				while(s[0] == ' '){


					// debug check
					if(first==0){
						cout << "scanning ROWS" << endl;
						first==1;
					}

					//look for the objective function
					if (s[1] != 'N'){

						rowCount++;
						// we are in a non-cost row
						cout << "in non-COST ROW" << endl;
						string rowName = s.substr(4, 10);

						std::stringstream trimmer;
						trimmer << rowName;
						trimmer >> rowName;

						//cout << "putting rowName "<< rowName<<" with " << rowName.size() <<endl;
						rowIndx[rowName] = numRowsRead;
						numRowsRead++;

						cout << "checking constraint type" << endl;
						if(s[1] == 'G'){
							//geq is greater than or equal to
							cout << "found geq constraint" << endl;
							ineqMap[rowName]="GEQ";

						}else if (s[1] == 'E'){
							//eql is equal to
							cout << "found eql constraint" << endl;
							ineqMap[rowName]="EQL";
							//offsetMap[rowName]=-1;

						}else if (s[1] == 'L'){
							//leq is less than or equal to
							cout << "found leq constraint" << endl;
							ineqMap[rowName]="LEQ";
						}

					} else {

						//the objective function, place it in row 0
						cout << "in COST ROW" << endl;
						//get the row name
						string rowName = s.substr(4, 10);

						std::stringstream trimmer;
						trimmer << rowName;
						trimmer >> rowName;

						//cout << "putting rowName "<< rowName<<" with " << rowName.size() <<endl;
						rowIndx[rowName] = 0;
						costName=rowName;

					}
					getline(file, s);
				}
			} else if (s.compare(0,7,"COLUMNS") == 0){


				cout << "in COLS" << endl;
				//do stuff for COLUMNS
				getline(file, s);
				
				while (s[0] == ' '){{

					//get the column (variable name)
					string colName = s.substr(4, 10);

					//strip off whitespace at the end
					std::stringstream trimmer;
					trimmer << colName;
					trimmer >> colName;

					//put the column into the map if it doesn't already exist
					//**you know it doesn't exist if the value of the key is 0
					//****yes, this is why the numCol is offset by 1 -_-;;
					if (colMap[colName] == 0){
						//cout << "updating COLS to " << numCols <<endl;
						colMap[colName] = numColsRead;
						colIndx[colName]= numColsRead;
						names[numColsRead] = colName;
						numColsRead++;
					}

					//get the row name
					string rowName = s.substr(14, 10);

					trimmer << rowName;
					trimmer >> rowName;
					//strip off the whitespace at the end

					//get the row/column value
					double val = atof (s.substr(24, 15).c_str());

					//We have all info to place data
					//generate a tuple containing (row, column, value)

					if(rowName.compare(costName)==0){
						cost[colIndx[colName]]=val;
					}else{
						A[rowIndx[rowName]][colIndx[colName]]=val;
					}

					// do make changes if we want
					if(ineqMap[rowName].compare("LEQ")==0){
						cout << "converting leq constraint for " << rowName<< endl;
						GridPos pos (rowIndx[rowName], colMap[colName] - 1, val);
						matrixPositions.push_back(pos);
					}else if (ineqMap[rowName].compare("GEQ")==0){
						GridPos pos (rowIndx[rowName], colMap[colName] - 1, val);
						matrixPositions.push_back(pos);
					}else if (ineqMap[rowName].compare("EQL")==0){
						cout << "converting eq constraint for " << rowName << " @ " << colName<<endl;
						GridPos pos (rowIndx[rowName], colMap[colName] - 1, val);
						matrixPositions.push_back(pos);


						/*
						if(offsetMap[rowName]<0){
							rowOffset++;
							offsetMap[rowName]=rowOffset+numRowsRead;
						}
						cout << "row is "<< offsetMap[rowName]<< " for " << rowName << " @ " << colMap[colName] - 1<< " with "<< (-1*val) <<endl;
						GridPos posx1 (offsetMap[rowName], colMap[colName] - 1, (-1.0*val));
						matrixPositions.push_back(posx1);
						*/
					}else{
						cout << "adding cost row element" << endl;
						//cost[colIndx[colName]]=val;
						GridPos pos (rowIndx[rowName], colMap[colName] - 1, val);
						matrixPositions.push_back(pos);
					}



					//check for another set
					if (s.length() > 40){
						//get the row name
						rowName = s.substr(39, 10);

						trimmer << rowName;
						trimmer >> rowName;
						//strip off whitespace at the end

						//get the value
						val = atof (s.substr(50, 15).c_str());

						//generate the tuple, add it to the list
						if(rowName.compare(costName)==0){
							cost[colIndx[colName]]=val;
						}else{
							A[rowIndx[rowName]][colIndx[colName]]=val;
						}

						if(ineqMap[rowName].compare("LEQ")==0){
							cout << "converting leq constraint for " << rowName<< endl;
							GridPos pos2 (rowIndx[rowName], colMap[colName] - 1, val);
							matrixPositions.push_back(pos2);
						}else if (ineqMap[rowName].compare("GEQ")==0){
							GridPos pos2 (rowIndx[rowName], colMap[colName] - 1, val);
							matrixPositions.push_back(pos2);
						}else if (ineqMap[rowName].compare("EQL")==0){
							cout << "converting eq constraint for " << rowName << " @ " << colName<<endl;
							GridPos pos2 (rowIndx[rowName], colMap[colName] - 1, val);
							matrixPositions.push_back(pos2);
							/*
							if(offsetMap[rowName]<0){
								rowOffset++;
								offsetMap[rowName]=rowOffset+numRowsRead;
							}
							cout << "row is "<< offsetMap[rowName]<< " for " << rowName << " @ " << colMap[colName] - 1<< " with "<< (-1*val) <<endl;
							GridPos posx1 (offsetMap[rowName], colMap[colName] - 1, (-1.0*val));
							matrixPositions.push_back(posx1);
							*/

						}else{
							cout << "adding cost row element" << endl;
							GridPos pos (rowIndx[rowName], colMap[colName] - 1, val);
							matrixPositions.push_back(pos);
						}


						//GridPos pos2 (rowIndx[rowName], colMap[colName] - 1, val);
					}
					
					getline(file, s);}

				}
			} else if (s.compare(0,3,"RHS") == 0){
				//rowOffset=1;
				int rhsIndx=0;
				cout << "in RHS" << endl;
				//do stuff for RHS
				getline(file, s);
				while (s[0] == ' '){
					//get the row name
					string rowName = s.substr(14, 10);

					std::stringstream trimmer;
					trimmer << rowName;
					trimmer >> rowName;
					//strip off whitespace

					//store the value
					double val = atof (s.substr(24, 15).c_str());

					//we know what row it goes in, have total number of columns
					//****remember that numCols is offset by +1
					//generate the tuple and add it to the list of (position, values)

					rhs[rowIndx[rowName]]=val;

					if(ineqMap[rowName].compare("LEQ")==0){
						cout << "converting leq constraint for " << rowName <<endl;
						GridPos pos (rowIndx[rowName], numColsRead - 1, val);
						matrixPositions.push_back(pos);
					}else if (ineqMap[rowName].compare("GEQ")==0){
						GridPos pos (rowIndx[rowName], numColsRead - 1, val);
						matrixPositions.push_back(pos);
					}else if (ineqMap[rowName].compare("EQL")==0){
						cout << "converting eq constraint for " << rowName<< endl;
						GridPos pos (rowIndx[rowName], numColsRead - 1, val);
						matrixPositions.push_back(pos);
						/*
						if(offsetMap[rowName]<0){
							rowOffset++;
							offsetMap[rowName]=rowOffset+numRowsRead;
						}
						GridPos posx1 (offsetMap[rowName], numColsRead - 1, (-1.0*val));
						matrixPositions.push_back(posx1);
						*/
					}

					//check for another value
					if (s.length() > 40){
						//get the row name
						rowName = s.substr(39, 10);

						//strip whitespace
						trimmer << rowName;
						trimmer >> rowName;

						//get the value
						val = atof (s.substr(50, 15).c_str());
						
						//generate the tuple and add it to the list

						rhs[rowIndx[rowName]]=val;

						if(ineqMap[rowName].compare("LEQ")==0){
							cout << "converting leq constraint" << endl;
							GridPos pos2 (rowIndx[rowName], numColsRead - 1, val);
							matrixPositions.push_back(pos2);
						}else if (ineqMap[rowName].compare("GEQ")==0){
							GridPos pos2 (rowIndx[rowName], numColsRead - 1, val);
							matrixPositions.push_back(pos2);
						}else  if (ineqMap[rowName].compare("EQL")==0){
							cout << "converting eq constraint" << endl;
							GridPos pos2 (rowIndx[rowName], numColsRead - 1, val);
							matrixPositions.push_back(pos2);
							/*
							if(offsetMap[rowName]<0){
								rowOffset++;
								offsetMap[rowName]=rowOffset+numRowsRead;
							}
							GridPos posx2 (offsetMap[rowName], numColsRead - 1, (-1.0*val));
							matrixPositions.push_back(posx2);
							*/
						}
					}
					getline(file, s);
				}
			} else if (s.compare(0,6,"BOUNDS") == 0){

				//rowOffset++;
				//getline(file, s);
				//while(s[0] == ' '){
					cout << "scanning BOUNDS" << endl;
					/*
					string colName = s.substr(14, 10);

					//strip off whitespace at the end
					std::stringstream trimmer;
					trimmer << colName;
					trimmer >> colName;

					double val = atof (s.substr(24, 15).c_str());

					//look for the objective function
					if (s.compare(1,2,"UP")==0){
						cout << "in UP BOUND: " << s << endl;
						GridPos posx1 (rowOffset + numRowsRead, colMap[colName] - 1, 1);
						matrixPositions.push_back(posx1);
						GridPos pos2 (rowOffset + numRowsRead, numColsRead - 1, val);
						matrixPositions.push_back(pos2);
						rowOffset++;
					} else if (s.compare(1,2,"LO")==0) {
						//the objective function, place it in row 0
						cout << "in LO BOUND: " << s << endl;
						GridPos posx1 (rowOffset + numRowsRead, colMap[colName] - 1, -1);
						matrixPositions.push_back(posx1);
						GridPos pos2 (rowOffset +  numRowsRead, numColsRead - 1, -1*val);
						matrixPositions.push_back(pos2);
						rowOffset++;
					}else{
						cout << "Somewhere else in BOUND" << endl;
					}

					getline(file, s);

				}*/
					getline(file, s);
			}else{
				getline(file, s);
			}
		}
	} else {
		//if no file gets open, return -> we don't want to simplex on 
		//empty stuff... bad will happen
		return 1;
	}
	file.close();
	
	ofstream ofile;

	cout << "rowIndx: "<< endl;
	int rowSize=0;
	int colSize=0;
	for(std::map<string,int>::iterator it = rowIndx.begin(); it != rowIndx.end(); ++it) {
	  string str =it->first;
	  cout << str << " with "<< str.size()<<" \n";
	  rowSize++;
	}
	rowSize+=rowOffset -1;
	cout << "rowIndx size = : "<< rowSize<<  endl;
	cout << "colMap: "<< endl;

	for(std::map<string,int>::iterator it = colMap.begin(); it != colMap.end(); ++it) {
	  string str =it->first;
	  cout << str << " with "<< str.size()<<" \n";
	  colSize++;
	}
	cout << "colMap size = : "<< colSize<<  endl;
	int vecSize = matrixPositions.size();
	cout << "size of vector is :" << vecSize<< endl;

	for (long index=0; index<vecSize; ++index) {
			try {
				cout << "Element " << index << ": row = " << matrixPositions.at(index).row << ", col=  " << matrixPositions.at(index).col << ", val= " << matrixPositions.at(index).val<< endl;

			}
			catch (exception& e) {
				cout << "Element " << index << ": index exceeds vector dimensions." << endl;
			}
		}


	// set slack variables
	int colOffset =0;
	int nextRow=1;
	int finished =-1;
	int cycle =1;
	int rhsCol=rowSize+ colSize;
	int preSlackColSize = colMap.size();
	cout<< " pre-slack size= "<< preSlackColSize<< endl;
	/*
	while(finished<0){
		for (long index=0; index<vecSize; ++index) {
				try {

					cout << "in cycle "<< cycle<< ": elements : " << index<< " : checking if col  :" <<matrixPositions.at(index).col << " == " << colSize << endl;
					if(matrixPositions.at(index).col == colSize){

						GridPos newPos(matrixPositions.at(index).row, rhsCol,matrixPositions.at(index).val);
						cout<< "inserting for row "<<  matrixPositions.at(index).row << endl;
						tempPos.push_back(newPos);
						cout << "checking if row  :" << nextRow<< " == " << matrixPositions.at(index).row<< endl;

						if(matrixPositions.at(index).row==nextRow){
							matrixPositions.at(index).col=colSize+colOffset;
							matrixPositions.at(index).val=1;
							colOffset++;
							nextRow++;
							std::stringstream ss;
							ss<<  "SLACK" << colOffset;
							colMap[ss.str()]= colSize+colOffset;
							cout << "completed slack at  :" << nextRow-1<< " of " << rowSize<< endl;

							if(nextRow==rowSize+1){
								int tempSize = tempPos.size();
								cout << "size of temp vector is :" << tempSize<< " vs " << vecSize << " for the master vector" << endl;
								for (long index2=0; index2<tempSize; ++index2) {
									matrixPositions.push_back(tempPos.at(index2));
								}
								finished =1;
							}
						}
					}

				}
				catch (exception& e) {
					cout << "Element " << index << ": index exceeds vector dimensions." << endl;
				}
		}
	}
	*/
	vecSize = matrixPositions.size();
	for (long index=0; index<vecSize; ++index) {
			try {
				cout << "Element " << index << ": row = " << matrixPositions.at(index).row << ", col=  " << matrixPositions.at(index).col << ", val= " << matrixPositions.at(index).val<< endl;

			}
			catch (exception& e) {
				cout << "Element " << index << ": index exceeds vector dimensions." << endl;
			}
		}

	ofstream outFile ("new.lp");

	int size = names.size();
	// put num dimensions
	outFile<< size <<endl;

	/*
	for(std::map<string,int>::iterator it = colMap.begin(); it != colMap.end(); ++it) {
	  string str =it->first;
	  cout << str << " with "<< colMap[str]<<" \n";
	  //colSize++;
	}
	*/
	// put xnames
	outFile<< "{ ";

	/*
	int start = 0;
	std::stringstream trimmer;
	finished =-1;
	vector<string> orderedCol;
	int nextCol=1;
	while(finished<0){

		for(std::map<string,int>::iterator it = colMap.begin(); it != colMap.end(); ++it) {
			  string str =it->first;
			  if(colMap[str]==nextCol){
				  if(start==0){
					start=1;
					 outFile<< str;
					 orderedCol.push_back(str);
				  }else{
					 outFile<< " , "<< str ;
					 orderedCol.push_back(str);
				  }
				  nextCol++;
				  if(nextCol==colMap.size()){
					  finished =1;
				  }
			  }

		}
	}
	*/

	numColsRead--;

	for(int i =1 ; i<= names.size();i++){
		if(i==1){
			outFile<< " "<<	names[i];
		}else{
			outFile<< " , "<< names[i];
		}

	}

	outFile<< " }"<< endl;

	// put x (bfs)
	outFile<< "( "<< size << " : { ";
	for(int i =1 ; i<= names.size();i++){
		if(i==1){
			outFile<< " -1";
		}else{
			outFile<< " , -1";
		}

	}
	outFile<< " } )"<< endl;

	// put f (cost)

	outFile<< "( "<< size << " : { ";
	for(int i =1 ; i<= names.size();i++){
		if(i==1){
			outFile <<" "<< cost[i];
		}else{
			outFile <<" , "<< cost[i];
		}

	}
	outFile<<" } )"<< endl;

	// put b (rhs)

	outFile <<"( "<< size << " : { ";

	for(int i =1 ; i<= rhs.size();i++){
		if(i==1){
			outFile<< " "<<	rhs[i];
		}else{
			outFile<< " , "<< rhs[i];
		}
	}

	outFile<< " } )"<< endl;

	// put A (matrix)

	outFile<<"( "<< numColsRead << " : { ";
	bool first=true;
	for(int i = 1; i<= A.size(); i++){

		if(i==1){

			outFile<<" ( "<< numColsRead << " : " << " { ";
			first = true;
			for(int j =1; j<= numColsRead; j++){

				if(first){
					if(A[i][j]!=0){
						outFile<< " "<< j;
						first=false;
					}
				}else{
					if(A[i][j]!=0){
						outFile<< " , "<< j;
					}
				}
			}
			outFile<< " } ; { ";
			first = true;
			for(int j =1; j<= numColsRead; j++){

				if(first){
					if(A[i][j]!=0){
						outFile<< " "<< A[i][j];
						first=false;
					}
				}else{
					if(A[i][j]!=0){
						outFile<< " , "<< A[i][j];
					}
				}
			}
			outFile<< " } ) ";

		}else{

			outFile<<" , ( "<< numColsRead << " : " << " { ";
			first = true;
			for(int j =1; j<= numColsRead; j++){

				if(first){
					if(A[i][j]!=0){
						outFile<< " "<< j;
						first=false;
					}
				}else{
					if(A[i][j]!=0){
						outFile<< " , "<< j;
					}
				}
			}

			outFile<< " } ; { ";
			first = true;
			for(int j =1; j<= numColsRead; j++){

				if(first){
					if(A[i][j]!=0){
						outFile<< " "<< A[i][j];
						first=false;
					}else{

					}
				}else{
					if(A[i][j]!=0){
						outFile<< " , "<< A[i][j];
					}
				}
			}
			outFile<< " } ) ";
		}
	}

	/*
	finished =-1;
	vecSize = matrixPositions.size();
	nextCol =preSlackColSize;
	int nextRHS = 1;
	start = 0;
	for(long i = preSlackColSize; i<rhsCol;++i){

		if(start==0){
			start=1;
			outFile<< i ;
		}else{
			outFile<< " , "<< i ;
		}

	}
	start=0;
	outFile<< " } ; { ";
	while(finished<0){
		for (long index=0; index<vecSize; ++index) {
					try {

						if(matrixPositions[index].col==rhsCol && matrixPositions[index].row == nextRHS){

							nextRHS++;
							if(start==0){
								start=1;
								outFile<<  matrixPositions[index].val;
							}else{
								outFile<<  " , " << matrixPositions[index].val;
							}

							if(nextRHS>rowSize){
								finished=1;
							}
						}

					}
					catch (exception& e) {
						cout << "Element " << index << ": index exceeds vector dimensions." << endl;
					}
			}
	}
    */
	outFile<< " } )"<< endl;


	return EXIT_SUCCESS;
	//-------------------------------------------
	//   CHANGING FREE VARIABLES NOT IMPLEMENTED
	//-------------------------------------------


	//Create the tableau object and populate it from the list of values created while
	//parsing the MPS file
	//Tableau tableau (numRows, numCols);

	/*inteate down the list (actually a vector) to populate the tableau with values
	for (int i = 0; i < matrixPositions.size(); i++){
		//this line looks scary, but its simply
		//tableau[row][col] = val;
		//where row and col come from the matrixPositions list
		tableau[matrixPositions[i].row][matrixPositions[i].col] = matrixPositions[i].val;
	}
	 */
	//fill in slack variables, we pass in the address since we want to actually modify
	//stuff in it at the address.
	//doSlack (&tableau);

	//-------------------------------------------
	// ONCE ALL DATA IS IN TABLEAU, SHOULD CLEAN UP VARS
	// FROM READING IN FILE
	//-------------------------------------------
	//print for sanity check
	//tableau.printMat();

	//Solver solver (&tableau);

	//cout << "unbounded? " << solver.isUnbounded() << '\n';
	//cout << "done? " << solver.check_if_done() << '\n';

	return 0;
}

