#include <iostream>
#include <fstream>
#include <string>
#include <cstring>
#include <map>
#include <stdlib.h>
#include <vector>
#include "GridPos.h"
#include <sstream>

using namespace std;



/*
void doSlack(Tableau* tableau){
	//store old number of columns, will have to use
	//this number to move the RHS column to the end
	int oldColNum = tableau->width() - 1;

	//add columns equals to the number of rows - 1 
	tableau->addCol(tableau->height() - 1);

	//move the RHS column to the end
	tableau->swapCols(oldColNum, tableau->width() - 1);

	//place in the swaps, starts at row=1, col=oldColNum
	for (int row = 1; row < tableau->height(); row++){
		(*tableau)[row][oldColNum + row - 1] = 1.0;
	}

	//you now have a standardized tableau matrix!
}
*/

int main(int argc, char* argv[]) {


	/*
	cout << "argc = " << argc << endl;
	for(int i = 0; i < argc; i++)
		cout << "argv[" << i << "] = " << argv[i] << endl;
	 */
	if(argc==1){
		cout << "Usage: input_MPS_file output_file_name \n";
	}


	//ifstream file;
	//file.open(argv[1]);
	//file.open("TESTPROB");
	string s;


	map<int, double> cost;
	map<int, string> names;
	map<string, int> colIndx;
	map<string, int> rowIndx;
	map<int, double> rhs;
	map<int, map<int, double> > A;

	int rowCount=0;
	int colCount=0;
	string costName;

	int numRowsRead = 1;
	int numColsRead = 1;


	map<string, int> colMap; //stores a map from the col name to its col number
	map<string, string> ineqMap;
	//map<string, string> bndMap;
	//map<string, int> offsetMap;
	vector<GridPos> matrixPositions; //a list of positions and values for the tableau
	vector<GridPos> tempPos;

	cout << "checking to see if file is open." << endl;
	int lineNum=0;
	int rowOffset = -1;
	if (true) {

		cout << "file is indeed open." << endl;

		//getline(file, s);
		getline(cin, s);
		while (s.compare(0,6,"ENDATA") != 0){
			lineNum++;
			//cout << s <<endl;

			/*
			 * In ROWS:
			 *
			 * information = length of rhs vector and constraints of rhs
			 */
			if (s.compare(0,4,"ROWS") == 0){

				//do stuff for ROW
				getline(cin, s);
				bool first = true;
				//int rowDeclarationIndent = 0;
				int rowConstraintIndent =0;
				int rowNameIndent = 0;
				while(s.compare(0,7,"COLUMNS") != 0){
					//cout << "in ROWS with " <<s<< endl;


					// debug check
					if(first){
						cout << "scanning ROWS" << endl;
						while(s[rowConstraintIndent]==' '&& rowConstraintIndent<10){
							rowConstraintIndent++;
						}
						rowNameIndent=rowConstraintIndent+1;
						while(s[rowNameIndent]==' ' && rowNameIndent<10){
							rowNameIndent++;
						}
						cout<< "row constraint indent is " << rowConstraintIndent<< " and row name indent is " << rowNameIndent << endl;
						first=false;;
					}

					//look for the objective function
					if (s[0] != 'N'){

						rowCount++;
						// we are in a non-cost row
						//cout << "in non-COST ROW" << endl;
						string rowName = s.substr(rowNameIndent, 10);

						std::stringstream trimmer;
						trimmer << rowName;
						trimmer >> rowName;

						//cout << "putting rowName "<< rowName <<" at index " << numRowsRead <<endl;
						rowIndx[rowName] = numRowsRead;
						numRowsRead++;

						//cout << "checking constraint type" << endl;
						if(s[0] == 'G'){
							//geq is greater than or equal to
							//cout << "found geq constraint" << endl;
							ineqMap[rowName]="GEQ";

						}else if (s[0] == 'E'){
							//eql is equal to
							//cout << "found eql constraint" << endl;
							ineqMap[rowName]="EQL";
							//offsetMap[rowName]=-1;

						}else if (s[0] == 'L'){
							//leq is less than or equal to
							//cout << "found leq constraint" << endl;
							ineqMap[rowName]="LEQ";
						}

					} else {

						//the objective function, place it in row 0
						//cout << "in COST ROW" << endl;
						//get the row name
						string rowName = s.substr(rowNameIndent, 10);

						std::stringstream trimmer;
						trimmer << rowName;
						trimmer >> rowName;

						//cout << "putting rowName "<< rowName<<" with " << rowName.size() <<endl;
						rowIndx[rowName] = 0;
						costName=rowName;

					}
					getline(cin, s);
				}
			} else if (s.compare(0,7,"COLUMNS") == 0){


				//do stuff for COLUMNS
				getline(cin, s);

				while (s[0] == ' '){

					//cout << "in COLS with " << s<< endl;
					//get the column (variable name)
					string colName = s.substr(4, 10);

					//strip off whitespace at the end
					std::stringstream trimmer;
					trimmer << colName;
					trimmer >> colName;

					//put the column into the map if it doesn't already exist
					//**you know it doesn't exist if the value of the key is 0
					//****yes, this is why the numCol is offset by 1 -_-;;
					if (colMap[colName] == 0){
						//cout << "putting in ColMap @ "<< colName <<" with index of " << numColsRead <<endl;
						colMap[colName] = numColsRead;
						//cout << "registering index of " << numColsRead << " with "<< colName <<endl;
						colIndx[colName]= numColsRead;
						//cout << "registering name " << colName << " at index of "<< numColsRead <<endl;
						names[numColsRead] = colName;
						numColsRead++;
					}

					//get the row name
					string rowName = s.substr(14, 10);

					trimmer << rowName;
					trimmer >> rowName;
					//strip off the whitespace at the end

					//get the row/column value
					double val = atof (s.substr(24, 15).c_str());

					//We have all info to place data
					//generate a tuple containing (row, column, value)

					//cout<< "compare "<< rowName << " from "<< costName << endl;
					if(rowName.compare(costName)==0){
						cost[colIndx[colName]]=val;
					}else{
						//cout<< " registering value of "<< val<< " with row " << rowName <<" @ "<< rowIndx[rowName] <<" and col "<<colName<<" @ "<< colIndx[colName]<< endl;
						A[rowIndx[rowName]][colIndx[colName]]=val;
					}

					// do make changes if we want
					if(ineqMap[rowName].compare("LEQ")==0){
						//cout << "converting leq constraint for " << rowName<< endl;
						GridPos pos (rowIndx[rowName], colMap[colName] - 1, val);
						matrixPositions.push_back(pos);
					}else if (ineqMap[rowName].compare("GEQ")==0){
						GridPos pos (rowIndx[rowName], colMap[colName] - 1, val);
						matrixPositions.push_back(pos);
					}else if (ineqMap[rowName].compare("EQL")==0){
						//cout << "converting eq constraint for " << rowName << " @ " << colName<<endl;
						GridPos pos (rowIndx[rowName], colMap[colName] - 1, val);
						matrixPositions.push_back(pos);


						/*
						if(offsetMap[rowName]<0){
							rowOffset++;
							offsetMap[rowName]=rowOffset+numRowsRead;
						}
						cout << "row is "<< offsetMap[rowName]<< " for " << rowName << " @ " << colMap[colName] - 1<< " with "<< (-1*val) <<endl;
						GridPos posx1 (offsetMap[rowName], colMap[colName] - 1, (-1.0*val));
						matrixPositions.push_back(posx1);
						*/
					}else{
						//cout << "adding cost row element" << endl;
						//cost[colIndx[colName]]=val;
						GridPos pos (rowIndx[rowName], colMap[colName] - 1, val);
						matrixPositions.push_back(pos);
					}



					//check for another set
					if (s.length() > 40){
						//get the row name

						//cout << "--------------> 2nd COL" << endl;
						rowName = s.substr(39, 10);

						trimmer << rowName;
						trimmer >> rowName;
						//strip off whitespace at the end

						//get the value
						val = atof (s.substr(50, 15).c_str());


						if (colMap[colName] == 0){
							//cout << "putting in ColMap @ "<< colName <<" with index of " << numColsRead <<endl;
							colMap[colName] = numColsRead;
							//cout << "registering index of " << numColsRead << " with "<< colName <<endl;
							colIndx[colName]= numColsRead;
							//cout << "registering name " << colName << " at index of "<< numColsRead <<endl;
							names[numColsRead] = colName;
							numColsRead++;
						}

						//generate the tuple, add it to the list
						if(rowName.compare(costName)==0){
							cost[colIndx[colName]]=val;
						}else{
							//cout<< " registering value of "<< val<< " with row " << rowName <<" @ "<< rowIndx[rowName] <<" and col "<<colName<<" @ "<< colIndx[colName]<< endl;
							A[rowIndx[rowName]][colIndx[colName]]=val;
						}

						if(ineqMap[rowName].compare("LEQ")==0){
							//cout << "converting leq constraint for " << rowName<< endl;
							GridPos pos2 (rowIndx[rowName], colMap[colName] - 1, val);
							matrixPositions.push_back(pos2);
						}else if (ineqMap[rowName].compare("GEQ")==0){
							GridPos pos2 (rowIndx[rowName], colMap[colName] - 1, val);
							matrixPositions.push_back(pos2);
						}else if (ineqMap[rowName].compare("EQL")==0){
							//cout << "converting eq constraint for " << rowName << " @ " << colName<<endl;
							GridPos pos2 (rowIndx[rowName], colMap[colName] - 1, val);
							matrixPositions.push_back(pos2);
							/*
							if(offsetMap[rowName]<0){
								rowOffset++;
								offsetMap[rowName]=rowOffset+numRowsRead;
							}
							cout << "row is "<< offsetMap[rowName]<< " for " << rowName << " @ " << colMap[colName] - 1<< " with "<< (-1*val) <<endl;
							GridPos posx1 (offsetMap[rowName], colMap[colName] - 1, (-1.0*val));
							matrixPositions.push_back(posx1);
							*/


						}else{
							cout << "adding cost row element" << endl;
							GridPos pos (rowIndx[rowName], colMap[colName] - 1, val);
							matrixPositions.push_back(pos);
							//cout << "<----------------end 2nd COL"<< endl;
						}
						//cout << "<----------------end 2nd COL"<< endl;

						//GridPos pos2 (rowIndx[rowName], colMap[colName] - 1, val);
					}
					
					getline(cin, s);
				}
			} else if (s.compare(0,3,"RHS") == 0){
				//rowOffset=1;
				int rhsIndx=0;

				string firstRHSName;
				bool firstRHS=true;
				//do stuff for RHS
				getline(cin, s);
				bool first=true;
				while (s[0] == ' '){
					//cout << "in RHS with " << s<< endl;
					//get the row name
					std::stringstream trimmer;
					string rhsName = s.substr(0, 10);

					trimmer << rhsName;
					trimmer >> rhsName;

					if(first){
						first=false;
						firstRHSName=rhsName;
					}

					firstRHS = (rhsName.compare(firstRHSName)==0);

					if(firstRHS){

						string rowName = s.substr(14, 10);

						trimmer << rowName;
						trimmer >> rowName;
						//strip off whitespace
						//cout << "RHS row name " << rowName<< endl;
						//store the value
						double val = atof (s.substr(24, 15).c_str());
						//cout << "RHS row value " << val<< endl;
						//we know what row it goes in, have total number of columns
						//****remember that numCols is offset by +1
						//generate the tuple and add it to the list of (position, values)

						//cout <<"inserting "<< rowName<< " into index to get index# "<< rowIndx[rowName] << " to insert "<< val << " into rhs" << endl;
						rhs[rowIndx[rowName]]=val;

						if(ineqMap[rowName].compare("LEQ")==0){
							//cout << "converting leq constraint for " << rowName <<endl;
							GridPos pos (rowIndx[rowName], numColsRead - 1, val);
							matrixPositions.push_back(pos);
						}else if (ineqMap[rowName].compare("GEQ")==0){
							GridPos pos (rowIndx[rowName], numColsRead - 1, val);
							matrixPositions.push_back(pos);
						}else if (ineqMap[rowName].compare("EQL")==0){
							//cout << "converting eq constraint for " << rowName<< endl;
							GridPos pos (rowIndx[rowName], numColsRead - 1, val);
							matrixPositions.push_back(pos);
							/*
							if(offsetMap[rowName]<0){
								rowOffset++;
								offsetMap[rowName]=rowOffset+numRowsRead;
							}
							GridPos posx1 (offsetMap[rowName], numColsRead - 1, (-1.0*val));
							matrixPositions.push_back(posx1);
							*/
						}

						//check for another value
						if (s.length() > 40){
							//get the row name
							rowName = s.substr(39, 10);

							//strip whitespace
							trimmer << rowName;
							trimmer >> rowName;
							//cout << "RHS row name " << rowName<< " from " << rhsName<< endl;
							//get the value
							val = atof (s.substr(50, 15).c_str());
							//cout << "RHS row value " << val<< " from " << rhsName<< endl;
							//generate the tuple and add it to the list
							//cout <<"inserting "<< rowName<< " into index to get index# "<< rowIndx[rowName] << " to insert "<< val << " into rhs" << endl;
							rhs[rowIndx[rowName]]=val;

							if(ineqMap[rowName].compare("LEQ")==0){
								//cout << "converting leq constraint" << endl;
								GridPos pos2 (rowIndx[rowName], numColsRead - 1, val);
								matrixPositions.push_back(pos2);
							}else if (ineqMap[rowName].compare("GEQ")==0){
								GridPos pos2 (rowIndx[rowName], numColsRead - 1, val);
								matrixPositions.push_back(pos2);
							}else  if (ineqMap[rowName].compare("EQL")==0){
								//cout << "converting eq constraint" << endl;
								GridPos pos2 (rowIndx[rowName], numColsRead - 1, val);
								matrixPositions.push_back(pos2);
								/*
								if(offsetMap[rowName]<0){
									rowOffset++;
									offsetMap[rowName]=rowOffset+numRowsRead;
								}
								GridPos posx2 (offsetMap[rowName], numColsRead - 1, (-1.0*val));
								matrixPositions.push_back(posx2);
								*/
							}
						}
					}
					getline(cin, s);
				}
			} else if (s.compare(0,6,"BOUNDS") == 0){

				//rowOffset++;
				//getline(file, s);
				//while(s[0] == ' '){
					cout << "scanning BOUNDS" << endl;
					/*
					string colName = s.substr(14, 10);

					//strip off whitespace at the end
					std::stringstream trimmer;
					trimmer << colName;
					trimmer >> colName;

					double val = atof (s.substr(24, 15).c_str());

					//look for the objective function
					if (s.compare(1,2,"UP")==0){
						cout << "in UP BOUND: " << s << endl;
						GridPos posx1 (rowOffset + numRowsRead, colMap[colName] - 1, 1);
						matrixPositions.push_back(posx1);
						GridPos pos2 (rowOffset + numRowsRead, numColsRead - 1, val);
						matrixPositions.push_back(pos2);
						rowOffset++;
					} else if (s.compare(1,2,"LO")==0) {
						//the objective function, place it in row 0
						cout << "in LO BOUND: " << s << endl;
						GridPos posx1 (rowOffset + numRowsRead, colMap[colName] - 1, -1);
						matrixPositions.push_back(posx1);
						GridPos pos2 (rowOffset +  numRowsRead, numColsRead - 1, -1*val);
						matrixPositions.push_back(pos2);
						rowOffset++;
					}else{
						cout << "Somewhere else in BOUND" << endl;
					}

					getline(file, s);

				}*/
					getline(cin, s);
			}else{
				getline(cin, s);
			}
		}
	} else {
		//if no file gets open, return -> we don't want to simplex on 
		//empty stuff... bad will happen
		return 1;
	}
	//file.close();
	
	ofstream ofile;

	/*
	cout << "rowIndx: "<< endl;
	int rowSize=0;
	int colSize=0;

	for(std::map<string,int>::iterator it = rowIndx.begin(); it != rowIndx.end(); ++it) {
	  string str =it->first;
	  cout << str << " with "<< str.size()<<" \n";
	  rowSize++;
	}
	rowSize+=rowOffset -1;
	cout << "rowIndx size = : "<< rowSize<<  endl;
	cout << "colMap: "<< endl;

	for(std::map<string,int>::iterator it = colMap.begin(); it != colMap.end(); ++it) {
	  string str =it->first;
	  cout << str << " with "<< str.size()<<" \n";
	  colSize++;
	}
	cout << "colMap size = : "<< colSize<<  endl;
	int vecSize = matrixPositions.size();
	cout << "size of vector is :" << vecSize<< endl;

	for (long index=0; index<vecSize; ++index) {
			try {
				cout << "Element " << index << ": row = " << matrixPositions.at(index).row << ", col=  " << matrixPositions.at(index).col << ", val= " << matrixPositions.at(index).val<< endl;

			}
			catch (exception& e) {
				cout << "Element " << index << ": index exceeds vector dimensions." << endl;
			}
		}
	 */
	int preSlackColSize = colMap.size();
	cout<< " pre-slack size= "<< preSlackColSize<< endl;

	ofstream outFile ("new.lp");

	int size = names.size();
	// put num dimensions
	outFile<< size <<endl;

	// put xnames
	outFile<< "{ ";

	//numColsRead--;


	cout<< "Putting " << size << " names" << endl;
	for(int i =1 ; i<= size;i++){
		if(i==1){
			outFile<< " "<<	names[i];
		}else{
			outFile<< " , "<< names[i];
		}

	}

	outFile<< " }"<< endl;

	cout<< "Putting " << size << " bfs" << endl;

	// put x (bfs)
	outFile<< "( "<< size << " : { ";

	for(int i =1 ; i<= names.size();i++){
			if(i==1){
				outFile<< " " << i-1;
			}else{
				outFile<< " , " << i-1;
			}

	}

	outFile<< " } ; { ";

	for(int i =1 ; i<= names.size();i++){
		if(i==1){
			outFile<< " -1";
		}else{
			outFile<< " , -1";
		}

	}
	outFile<< " } )"<< endl;

	// put f (cost)

	cout<< "Putting " << size << " cost" << endl;

	outFile<< "( "<< size << " : { ";

	bool first = true;
	for(int i =1 ; i<= size;i++){
		if(first){
			double value = cost[i];
			if(value!=0){
				outFile <<" "<< i-1;
				first=false;
			}
		}else{
			double value = cost[i];
			if(value!=0){
				outFile <<" , "<< i-1;
			}
		}

	}

	outFile<< " } ; { ";
	for(int i =1 ; i<= size;i++){
		if(i==1){
			outFile <<" "<< cost[i];
		}else{
			outFile <<" , "<< cost[i];
		}

	}
	outFile<<" } )"<< endl;

	// put b (rhs)
	//int rhsSize = rhs.size();

	//numRowsRead--;
	cout<< "Putting " << numRowsRead-1 << " constraints " << endl;

	outFile <<"( "<< numRowsRead-1 << " : { ";

	first = true;
	for(int i =1 ; i< numRowsRead;i++){

		if(first){
			double value = rhs[i];
			if(value!=0){
				outFile <<" "<< i-1;
				first=false;
			}
		}else{
			double value = rhs[i];
			if(value!=0){
				outFile <<" , "<< i-1;
			}
		}

	}

	outFile<< " } ; { ";

	for(int i =1 ; i< numRowsRead;i++){

		//cout << "test in "<< i << endl;
		if(i==1){
			outFile<< " "<<	rhs[i];
		}else{
			outFile<< " , "<< rhs[i];
		}
	}

	outFile<< " } )"<< endl;

	// put A (matrix)
	//int rows = A.size();
	cout<< "Putting " << numColsRead-1 << " x " << numRowsRead-1 << " A matrix" << endl;

	/*
	for(int i=1; i<numRowsRead;i++){
		for(int j =1; j<numColsRead; j++){
			cout<<" i = " << i << ", j= " << j << " , A[i][j] = " << A[i][j] << endl;
		}
	}
	*/

	outFile<<"( "<< numColsRead-1 << " : { ";
	first=true;
	for(int i = 1; i <numRowsRead; i++){

		if(i%1000==0){
			cout<< " at row # " << i<< endl;
		}
		if(i==1){

			outFile<<" ( "<< numColsRead-1 << " : " << " { ";

			first = true;
			for(int j =0; j< A[i].size(); j++){

				if(first){
					if(A[i][j]!=0){
						outFile<< " "<< j-1;
						first=false;
					}
				}else{
					if(A[i][j]!=0){
						outFile<< " , "<< j-1;
					}
				}
			}
			outFile<< " } ; { ";
			first = true;
			for(int j =1; j< A[i].size(); j++){

				if(first){
					if(A[i][j]!=0){
						outFile<< " "<< A[i][j];
						first=false;
					}
				}else{
					if(A[i][j]!=0){
						outFile<< " , "<< A[i][j];
					}
				}
			}
			outFile<< " } ) ";

		}else{

			outFile<<" , ( "<< numColsRead-1 << " : " << " { ";
			first = true;
			for(int j =0; j< A[i].size(); j++){

				if(first){
					if(A[i][j]!=0){
						outFile<< " "<< j-1;
						first=false;
					}
				}else{
					if(A[i][j]!=0){
						outFile<< " , "<< j-1;
					}
				}
			}

			outFile<< " } ; { ";
			first = true;
			for(int j =0; j< A[i].size(); j++){

				if(first){
					if(A[i][j]!=0){
						outFile<< " "<< A[i][j];
						first=false;
					}else{

					}
				}else{
					if(A[i][j]!=0){
						outFile<< " , "<< A[i][j];
					}
				}
			}
			outFile<< " } ) ";
		}

		//cout<< "looking at row # " << i <<endl;
		/*
		if(i==1){

			outFile<<" ( "<< numColsRead-1 << " : " << " { ";

			first = true;
			for(int j =1; j< numColsRead; j++){

				if(first){
					if(A[i][j]!=0){
						outFile<< " "<< j-1;
						first=false;
					}
				}else{
					if(A[i][j]!=0){
						outFile<< " , "<< j-1;
					}
				}
			}
			outFile<< " } ; { ";
			first = true;
			for(int j =1; j< numColsRead; j++){

				if(first){
					if(A[i][j]!=0){
						outFile<< " "<< A[i][j];
						first=false;
					}
				}else{
					if(A[i][j]!=0){
						outFile<< " , "<< A[i][j];
					}
				}
			}
			outFile<< " } ) ";

		}else{

			outFile<<" , ( "<< numColsRead-1 << " : " << " { ";
			first = true;
			for(int j =1; j< numColsRead; j++){

				if(first){
					if(A[i][j]!=0){
						outFile<< " "<< j-1;
						first=false;
					}
				}else{
					if(A[i][j]!=0){
						outFile<< " , "<< j-1;
					}
				}
			}

			outFile<< " } ; { ";
			first = true;
			for(int j =1; j< numColsRead; j++){

				if(first){
					if(A[i][j]!=0){
						outFile<< " "<< A[i][j];
						first=false;
					}else{

					}
				}else{
					if(A[i][j]!=0){
						outFile<< " , "<< A[i][j];
					}
				}
			}
			outFile<< " } ) ";
		}*/
	}


	outFile<< " } )"<< endl;


	return EXIT_SUCCESS;

	return 0;
}

