#include "ConicSamplingPolytopeProjection.h"

ConicSamplingPolytopeProjection::ConicSamplingPolytopeProjection():
  hitChecker(1e-6)
{}

ConicSamplingPolytopeProjection::ConicSamplingPolytopeProjection(char * fname):
  hitChecker(1e-6)
{
  ifstream fin(fname);
  load(fin);

  //  addPositivityConstraints();

  checkFeasibility();
}

void ConicSamplingPolytopeProjection::load(istream & fin)
{
  fin >> n;
  fin >> names;
  fin >> x;

  fin >> x0; // ideal point
  fin >> b;
  fin >> A;

  iterations = 0;

  normalizeLP();
}

void ConicSamplingPolytopeProjection::recalculateF()
{
  f = x0 - x;
}

void ConicSamplingPolytopeProjection::checkFeasibility() const
{
  Set errors = ((A*x-b) < 0.0);

  if ( ! errors.isEmpty() )
    {
      cerr << "Warning-- didn't start feasible." << endl;
      cerr << "Error constraints are " << errors << endl;
      cerr << "Amount off is " << (A*x-b)[errors] << endl;
      exit(1);
    }  
}

Vector ConicSamplingPolytopeProjection::solve()
{
  recalculateF();
  for (;;)
    {
      iterations++;
      if ( ! descentStep() )
	break;

      if ( ! randomStep() )
	break;
    }

  return x;
}

Vector ConicSamplingPolytopeProjection::getProjection() const
{
  // Linear projection based method
  // multiply vector first to ensure O(n^2) for multiplication
  //  return f - S.transpose()*(SSTransInverse*(S*f));

  // Gram-Schmidt based method
  return gs.projectOrthogonal(f);
}

void ConicSamplingPolytopeProjection::addConstraint(int index)
{
  // Linear projection based method
  //  addConstraintLinearProjection(index);

  // Gram-Schmidt based method
  addConstraintGramSchmidt(index);
}

bool ConicSamplingPolytopeProjection::descentStep()
{
  Vector p = getProjection();

  int count = 0;

  while ( hitChecker.isNonzero( p.norm() / p.size() ) )
    {
      int i = advance(p);
      
      // stop if no constraint is hit
      if ( i == -1 )
	return false;

      addConstraint( i );

      p = getProjection();

      count++;
    }
  return true;
}

Matrix ConicSamplingPolytopeProjection::getSpanningVectors() const
{
  Matrix sTrans = S.transpose();
  Matrix result =  sTrans.RRE();

  return result;
}

bool ConicSamplingPolytopeProjection::randomStep()
{
  Set adj = A*x-b == 0;


  Matrix adjA = A[adj];

  Matrix sp = getSpanningVectors();

  // test for optimality--
  // if all spanning vectors have a negative product with f, then
  // you are optimal.
  Set supporting = sp*f > 0;
  Set opposing = sp*f <= 0;

  if ( supporting.isEmpty() )
    {
      // there are only opposing
      // --> optimal; quit
     
      //      cerr << x << endl;
 
      clear();
      return false;
    }

  // now only sample from the ones that can extend

  Set extendable = (adjA * sp.transpose()).transpose().succEq(0.0);

  Set suppExtendable = supporting & extendable;
  Set oppExtendable = opposing & extendable;


  Array<double> suppCoefs( suppExtendable.size() );
  Array<double> oppCoefs( oppExtendable.size() );

  Random::fillRandomUniform(suppCoefs, 0.0, 1.0);
  Random::fillRandomUniform(oppCoefs, 0.0, 1.0);

  Vector direction;
  Vector suppNet, oppNet;

  if ( ! suppExtendable.isEmpty() && ! oppExtendable.isEmpty() )
    {
      suppNet = Vector(suppCoefs)*sp[suppExtendable];
      oppNet = Vector(oppCoefs)*sp[oppExtendable];

      // choose a theta such that ( theta suppNet + (1-theta) oppNet ) * f > 0
      double thetaMin = -oppNet*f/( (suppNet-oppNet)*f );
      double theta = Random::uniform( thetaMin, 1.0 );

      direction = theta * suppNet + (1-theta) * oppNet;

      clear();
    }
  else if ( ! suppExtendable.isEmpty() )
    {
      direction = suppNet;
      clear();
    }
  else
    {
      // performance note: it will be possible to get the appropriate
      // edge in n^2 using matrix inverse updating

      // Bland's rule:
      // I think this should choose the first positive spanning
      // vector, and so should be consistent with Bland's rule
      direction = sp[ supporting[0] ];

      // this should be, by definition, a singleton set
      int dropped = currentGliders[ ( A[currentGliders] * direction > 0 )][0];

      // remove the dropped constraint it from the new glider set
      Set newGliders = currentGliders.without( Set::SingletonSet(dropped) );

      clear();
      currentGliders = newGliders;

      // add everything but the dropped constraint 
      for ( Set::Iterator iter = newGliders.begin(); iter != newGliders.end(); iter++ )
	{
	  addConstraint( *iter );
	}
    }

  int i = advance(direction);

  if ( i == -1 )
    return false;

  addConstraint(i);

  return true;
}

void ConicSamplingPolytopeProjection::clear()
{
  gs = GramSchmidt();
  S = Matrix();
  SSTransInverse = Matrix();
  currentGliders = Set();
}

int ConicSamplingPolytopeProjection::advance(const Vector & p)
{
  // advances along direction and return the index of the
  // constraint that it hits

  Set opposing = (A*p) < 0;

  double gamma_projection = (x0 - x)*p / pow(p.norm(), 2);

  if ( opposing.isEmpty() )
    {
      x = x + gamma_projection * p;
      return -1;
    }

  // on opposing set:
  // A (x + gamma p) = b
  // find the smallest gamma
  Matrix temp = A[opposing];
  Vector gamma = ( Vector( b[opposing] ) - temp*x ) / ( temp*p );

  double gammaStar = gamma.min();

  gammaStar = min(gammaStar, gamma_projection);

  Set indices = gamma.argmin();

  x = x + gammaStar * p;
  recalculateF();

  int firstElement = (opposing[ indices ])[0];

  if ( ! (Set::SingletonSet(firstElement) & currentGliders ).isEmpty() )
    {
      cerr << "\tWarning: adding current glider to set..." << endl;
      cerr << "\t" << firstElement << " added with " << currentGliders.size() << " current members" << endl;
      cerr << "\tIt opposed with " << p*A[firstElement] << " and was reached with gamma = " << gammaStar << endl;
      getchar();
    }

  currentGliders |= Set::SingletonSet( firstElement );

  return firstElement;
}

void ConicSamplingPolytopeProjection::addConstraintGramSchmidt(int index)
{
  const Vector & row = A[ index ];
  gs.add( row );
  S.add( row );
}

void ConicSamplingPolytopeProjection::addConstraintLinearProjection(int index)
{
  /***
   // lame method
  S.add(A[index]);
  SSTransInverse = (S * S.transpose() ).inverse();
  return f - S.transpose()*(SSTransInverse*(S*f));
  ***/

  // sherman-morrison method
  const Vector & rowL = A[index];
  S.add( rowL );

  if ( S.numRows() == 1 )
    {
      SSTransInverse = ( S*S.transpose() ).inverse();
    }
  else
    {
      // compute the updated inverse of (S * S^T)
      SSTransInverse.embedInIdentity();

      Vector uA( SSTransInverse.numRows() );
      uA.addElement( SSTransInverse.numRows()-1 , 1.0 );

      Vector uB = S*rowL;
      
      SSTransInverse = ShermanMorrison(SSTransInverse, uB, uA);
      SSTransInverse = ShermanMorrison(SSTransInverse, uA, uB);

      // remove the corner element, which is added twice

      SSTransInverse = ShermanMorrison(SSTransInverse, uA, (-1.0 - 1.0*rowL*rowL)*uA);
    }
}

void ConicSamplingPolytopeProjection::outputMathematica(char * fname) const
{
  ofstream outA(fname);

  outA << "A = " << A.unpack() << ";" << endl;
  outA << "b = " << b.unpack() << ";" << endl;
  outA << "x0 = " << x0.unpack() << ";" << endl;
  outA << "xInit = " << x.unpack() << ";" << endl;

  outA.close();

  string instr = string("sed s/e/*10^/g ")+string(fname)+string(" > ")+string("/tmp/tmp_t");
  system( instr.c_str() );

  string instr2 = "mv /tmp/tmp_t " + string(fname);
  system( instr2.c_str() );

  ofstream outB(fname, ios::app);
  outB << "x = Table[ Subscript[v, i], {i, 1, Length[ A[[1]] ]}];" << endl;
  outB << "xAndInitialValues = Table[ { Subscript[v, i], xInit[[i]] }, {i, 1, Length[ A[[1]] ]}];" << endl;
  outB << "Timing[ FindMinimum[ { (x-x0).(x-x0), Table[A[[i]].x >= b[[i]], {i,1,Length[A]}]}, xAndInitialValues ] ]" << endl;

  //  fout << "x = Table[Subscript[v, i], {i, 1, Length[f]}];" << endl;
  //  fout << "Maximize[{f.x, Table[a[[i]].x <= b[[i]], {i, 1, Length[a]}]}, x]" << endl;
  //  fout << "( x /. Last[%] ). f" << endl;
}

void ConicSamplingPolytopeProjection::printResult() const
{
  cout << "x = " << x << endl;
  cout << "Dist^2 = " << pow((x - x0).norm(), 2) << endl;
  cout << "Iterations = " << iterations << endl;
  cout << endl;
}
