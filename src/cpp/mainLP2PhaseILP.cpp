#include <iostream>
#include <math.h>
#include <fstream>
#include <string>
#include <sstream>

#include "Matrix.h"

using namespace std;

int main(int argc, char**argv)
{
  if ( argc == 2 )
    {
      ifstream fin(argv[1]);

      int n;
      Vector x, f, b;
      Matrix a;

      Array<string> names;

      fin >> n >> names >> x >> f >> b >> a;
  
      // for positivity of variables

      int k = a.numRows();

      Matrix newA(n+k);
      Array<double> newB = b.unpack();

      for (int i=0; i<k; i++)
	{
	  // add the slack vars to each row
	  Array<double> row = a[i].unpack();
	  Array<double> new_row(n+k, 0.0);

	  // copy in old constraint
	  for (int j=0; j<n; j++)
	    new_row[j] = row[j];

	  new_row[n+i] = 1.0;
	  
	  newA.add( Vector(new_row) );
	}

      // constrain the slack vars >= 0
      for (int i=0; i<k; i++)
	{
	  // add the slack vars to each row
	  Array<double> new_row(n+k, 0.0);

	  new_row[n+i] = 1.0;

	  newA.add(new_row);
	  newB.add(0.0);
	}

      Array<double> newF(n+k, 0.0);
      for (int i=0; i<k; i++)
	{
	  newF[n+i] = -1.0;
	}

      // set the slack variables to large values to guarantee feasibility
      Array<double> newX = Array<double>(n+k, 0.0);
      for (int i=0; i<k; i++)
	{
	  newX[n+i] = 10000.0;
	}

      // add names for the slack variables
      for (int i=0; i<k; i++)
	{
	  ostringstream ost;
	  ost << i;
	  names.add("slack" + ost.str());
	}

      cout << n+k << endl;
      cout << names << endl;
      cout << Vector(newX) << endl;
      cout << Vector(newF) << endl;
      cout << Vector(newB) << endl;
      cout << newA << endl;
    }
  else
    {
      cerr << "usage: PhaseI-Maker <LP-file>" << endl << endl;
    }
  return 0;
}


