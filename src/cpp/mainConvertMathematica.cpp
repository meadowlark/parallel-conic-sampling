#include <iostream>
#include <math.h>
#include <fstream>
#include <string>
#include "ConicSampling.h"

using namespace std;

int main(int argc, char**argv)
{
  if ( argc == 3 )
    {
      cout.precision(15);
      ConicSampling primal(argv[1]);
      primal.outputMathematica(argv[2]);
    }
  else
    {
      cerr << "usage: LP2Mathematica <LP filename> <Mathematica filename>" << endl << endl;
    }
  return 0;
}


